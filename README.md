# Instructions

You'll need to have node and npm installed.

## Install dependencies

```
npm install
```

## Generate simplified geojson health maps

```
node generate.js
```

## I just want the map files

We uploaded some we baked earlier in the `saved` folder.
