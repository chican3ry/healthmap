const fetch = require('node-fetch');
const fs = require('fs');
const mapshaper = require('mapshaper')
const https = require('https');

const DOH_NI_TRUST_BOUNDARIES = "https://www.opendatani.gov.uk/dataset/0b04b46c-49af-45d5-b277-91b10937a01b/resource/645f8eef-8813-47a9-bb1e-a4932ada721a/download/trustboundaries.geojson"
const LHB_WALES_BOUNDARIES = "https://lle.gov.wales/services/wfs?version=1.0.0&request=GetFeature&typeName=inspire-wg:local_health_boards_low_water_mark_post_apr_2019&outputFormat=application%2Fjson"
const STP_ENGLAND_BOUNDARIES = 'http://geoportal1-ons.opendata.arcgis.com/datasets/5e33fd1436114a19b335ed07076a4c5b_0.geojson?outSR={%22latestWkid%22:27700,%22wkid%22:27700}'
const CCG_ENGLAND_BOUNDARIES = 'https://opendata.arcgis.com/datasets/5252644ec26e4bffadf9d3661eef4826_2.geojson'

// Scotland's data access for this is.. a bit of a journey
async function getScotland() {
	const NHS_HBs_SCOTLAND = await fetch("https://maps.gov.scot/server/rest/services/ScotGov/HealthSocialCare/MapServer/0?f=json")
		.then(response=>response.json())
		.then(r=>r.drawingInfo.renderer.uniqueValueInfos)
		.then(vals=>vals.map(({value})=>value))
	var collection = {type: "FeatureCollection",features: []}
	maps = await Promise.all(
		NHS_HBs_SCOTLAND.map(
			board=>{
				console.log(`Getting ${board}`);
				return fetch(`https://maps.gov.scot/server/rest/services/ScotGov/HealthSocialCare/MapServer/0/query?where=&text=${board}&outFields=OBJECTID%2CHBCode%2CHBName%2CShape&f=geojson`)
					.then(r=>r.json())
					.then(j=>j['features'][0])
			}
		)
	);
	collection['features'] = maps;
	const simplified = await mapshaper.applyCommands('-i input.geojson -simplify 1% -o output.geojson',{'input.geojson':collection});
	return simplified['output.geojson'];
}

async function getEngland() {
	const gj = await fetch(STP_ENGLAND_BOUNDARIES)
		.then(response=>response.json()); // This will be the new variety
	const gj_ccs = await fetch(CCG_ENGLAND_BOUNDARIES)
		.then(response=>response.json());
	console.log("Simplifying CCG map");
	const simplified = await mapshaper.applyCommands('-i input.geojson -simplify 10% -o output.geojson',{'input.geojson':gj_ccs})
	return simplified['output.geojson'];
}

async function getWales() {
	// Welsh govt data cert has an invalid leaf node, DOH.
	const httpsAgent = new https.Agent({
		rejectUnauthorized: false,
	});


	const gj = await fetch(LHB_WALES_BOUNDARIES, {agent:httpsAgent})
		.then(response=>response.json());

	// Welsh government also publish these stats in Ordnance Survey rather than Lat/Lon
	var epsg = require('epsg')
	var reproject = require('reproject')
	console.log("Reprojecting Welsh health boards from Ordnance Survey to Lat/Lon")
	const gj_wgs = reproject.toWgs84(gj,undefined,epsg);
	console.log("Simplifying Welsh Health Board GeoJSON")
	const simplified = await mapshaper.applyCommands('-i input.geojson -simplify 1% -o output.geojson',{'input.geojson':gj_wgs});
	return simplified['output.geojson'];
}

async function getNI() {
	const gj = await fetch(DOH_NI_TRUST_BOUNDARIES)
		.then(r=>r.json());
	const simplified = await mapshaper.applyCommands('-i input.geojson -simplify 1% -o output.geojson',{'input.geojson':gj});
	return simplified['output.geojson'];
}

(async () => {
	// var scotmaps = await getScotland();
	// console.log(scotmaps);
	fs.writeFileSync('saved/scotland.geojson',await getScotland())
	fs.writeFileSync('saved/england.geojson',await getEngland())
	fs.writeFileSync('saved/wales.geojson',await getWales())
	fs.writeFileSync('saved/northern_ireland.geojson',await getNI())
	// console.log(await getScotland())
})()

